# Generated by Django 2.2.7 on 2019-12-01 13:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produto', '0006_auto_20191130_2316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produto',
            name='imagem',
            field=models.ImageField(blank=True, null=True, upload_to='produto_imagens/%Y/%m'),
        ),
    ]
