# E-commerce

## Como contribuir? ##

```
git clone git@github.com:Hp2501/Ecommerce.git
cd ecommerce
poetry shell
poetry install
python contrib/env_gen.py
python manage.py migrate
python manage.py loaddata data.json
python manage.py createsuperuser
python manage.py runserver
```
---

## O que ainda falta incluir no Projeto?

- [] `Método de Pagamento`
- [] `Calculo de Frete`
- [] `Cupons de Descontos`


## Imagens do Projeto


![Index](img/index.png)
![Detalhes do Produto](img/detalhes_produto.png)
![Carrinho](img/carrinho.png)
![Cadastro e Login](img/cadastro_login.png)
![Atualizar Cadastro](img/atualizar_cadastro.png)
![Resumo da Compra](img/resumo_compra.png)
![Pagamento](img/pagamento.png)


